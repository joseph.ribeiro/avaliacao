package br.edu.cest.Avalicao;

public class BOOK {
	public String Author = "";
	public String Edition = "";
	public String Volume = "";
	
	public String getAuthor() {
		return Author;
	}
	
	public void setAuthor(String author) {
		Author = author;
	}
	
	public String getEdition() {
		return Edition;
	}
	
	public void setEdition(String edition) {
		Edition = edition;
	}
	
	public String getVolume() {
		return Volume;
	}
	
	public void setVolume(String volume) {
		Volume = volume;
	}
	

}
