package br.edu.cest.Avalicao;

public class ITEM {
	public String Title  = "";
	public String Publisher = "";
	public String YearPublisher = "";
	public int ISBN;
	public int Price;
	
	public String getTitle() {
		return Title;
	}
	
	public void setTitle(String title) {
		Title = title;
	}
	
	public String getPublisher() {
		return Publisher;
	}
	
	public void setPublisher(String publisher) {
		Publisher = publisher;
	}
	
	public String getYearPublisher() {
		return YearPublisher;
	}
	
	public void setYearPublisher(String yearPublisher) {
		YearPublisher = yearPublisher;
	}
	
	public int getISBN() {
		return ISBN;
	}
	
	public void setISBN(int iSBN) {
		ISBN = iSBN;
	}
	
	public int getPrice() {
		return Price;
	}
	
	public void setPrice(int price) {
		Price = price;
	}

}
